<?php
/*
Plugin Name: The Mindful Index by The Mindful Collective
Plugin URI: https://globalbay.design
Description: A visualisation tool that allows users to display their Mindful Index score on your website.
Version: 1.0.0
Author: Global Bay
Author URI: https://globalbay.design/plugins/mindful-index-plugin/
License: MIT License
*/

defined('ABSPATH') or die('Access Denied');

define('QUESTIONAIR_PATH', plugin_dir_path(__FILE__));
define('QUESTIONAIR_URL', plugin_dir_url( __FILE__ ));
define('QUESTIONAIR', plugin_basename( __FILE__ ));


require_once dirname(__FILE__).'/assets/html2pdf/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;


require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

class Questionair_chart_visualization {
	public function __construct() {
		//add_action( 'admin_init', array( &$this, 'registerSettings' ) );
		add_action( 'admin_menu', array( &$this, 'adminPanelsAndMetaBoxes' ) );
		

		add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts_plugin'));
		add_action('wp_enqueue_scripts', array(&$this, 'load_my_scripts'));

		add_action('wp_ajax_save_all_questionair_result', array($this, 'save_all_questionair_result'));
		add_action('wp_ajax_nopriv_save_all_questionair_result', array($this, 'save_all_questionair_result'));

		add_action('wp_ajax_print_all_answers_graph', array($this, 'print_all_answers_graph'));
		add_action('wp_ajax_nopriv_print_all_answers_graph', array($this, 'print_all_answers_graph'));

		add_action('wp_ajax_update_all_questionair', array($this, 'update_all_questionair'));
		add_action('wp_ajax_nopriv_update_all_questionair', array($this, 'update_all_questionair'));
		
		add_action('wp_ajax_questionair_proceed_to_survey', array($this, 'questionair_proceed_to_survey'));
		add_action('wp_ajax_nopriv_questionair_proceed_to_survey', array($this, 'questionair_proceed_to_survey'));
		
		add_action('wp_ajax_reset_all_answers_graph', array($this, 'reset_all_answers_graph'));
		add_action('wp_ajax_nopriv_reset_all_answers_graph', array($this, 'reset_all_answers_graph'));
		
	}

	function admin_enqueue_scripts_plugin(){
		
		/*wp_enqueue_style('custom-css-js.css', plugins_url('assets/style.css', __FILE__));
		wp_enqueue_style('phlox-codemirror-css', plugins_url('assets/lib/codemirror.css', __FILE__));
		wp_enqueue_script('phlox-codemirror-editor-css', plugins_url('assets/mode/css/css.js', __FILE__ ));
		wp_enqueue_script('phlox-codemirror-editor-js', plugins_url('assets/mode/javascript/javascript.js', __FILE__ ));*/
	}

	function load_my_scripts(){
		wp_enqueue_script( 'questionair-d3-js', plugins_url('assets/d3.min.js', __FILE__));
		wp_enqueue_script('questionair-radar-js', plugins_url('assets/RadarChart.js', __FILE__ ));
		wp_enqueue_script('questionair-radarScript-js', plugins_url('assets/RadarScript.js', __FILE__ ));
		wp_enqueue_style('custom-css', plugins_url('assets/style.css', __FILE__ ));
		wp_enqueue_script('questionair-htmltocanvas', plugins_url('assets/html2canvas.min.js', __FILE__ ));
		wp_enqueue_script('questionair-canvastoimage', plugins_url('assets/canvas2image.js', __FILE__ ));
	}

	function adminPanelsAndMetaBoxes() {
    	add_menu_page( 
    		'Questionair Plugin',
    	 	'Mindfull Index', 
    	 	'manage_options', 
    	 	'questionair_plugin', 
    	 	array($this, 'all_questions_page'), 
    	 	QUESTIONAIR_URL.'assets/Plugin_Logo.png', 
    	 	110 
    	 );
        /*add_submenu_page( 
            'questionair_plugin', 
            'All Answers', 
            'All Answers', 
            'manage_options', 
            'all-answers',
            array($this, 'all_answers_page'),
            ''
        );*/
	}

	public function all_questions_page(){
    	require_once QUESTIONAIR_PATH.'templates/questionair_template.php';
    }

    public function all_answers_page(){
    	require_once QUESTIONAIR_PATH.'templates/all_answers_template.php';
    }

	public function save_all_questionair_result(){
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'questionair_answers';
		if (get_current_user_id()) {
			$Expdata = json_encode($_POST["allArrExp"]);
			$Realdata = json_encode($_POST["allArrReal"]);

			$AllExpdata = json_encode($_POST["allResExp"]);
			$AllRealdata = json_encode($_POST["allResReal"]);

			$userId = get_current_user_id();
			$check = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `userId` = $userId",""));
			if(count($check) > 0){
				$wpdb->update( 
					$table_name, 
					array(
						'exp_answers' => $Expdata,	
						'real_answers' => $Realdata,
						'all_exp_answers' => $AllExpdata,
						'all_real_answers' => $AllRealdata
					), 
					array( 'userId' => $userId ), 
					array( 	
						'%s',	
						'%s'	
					), 
					array( '%d' ) 
				);
			}else{
				$wpdb->insert( 
					$table_name, 
					array( 
						'userId' => $userId, 
						'exp_answers' => $Expdata,
						'real_answers' => $Realdata,
						'all_exp_answers' => $AllExpdata,
						'all_real_answers' => $AllRealdata 
					), 
					array( 
						'%d', 
						'%s', 
						'%s' 
					) 
				);
			}

		}
		die("done");
	}

	public function update_all_questionair(){
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'questionair_answers';
		$userId = get_current_user_id();
		$check = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `userId` = $userId",""));
		if(count($check) > 0){
			$wpdb->update( 
				$table_name, 
				array(
					'content_view' => "Yes"
				), 
				array( 'userId' => $userId ), 
				array( 	
					'%s',	
					'%s'	
				), 
				array( '%d' ) 
			);
		}else{
			$wpdb->insert( 
				$table_name, 
				array( 
					'userId' => $userId,
					'content_view' => "Yes" 
				), 
				array( 
					'%d', 
					'%s' 
				) 
			);
		}
	
		die("done");
	}

	public function print_all_answers_graph(){

		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'questionair';

		$highest = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Highest Purpose and Vision'",""));

		$planet = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Impact on the Planet'",""));

		$society = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Impact on Society'",""));

		$customer = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Customer and Community'",""));

		$leadership = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Leadership and Culture'",""));

		$employee = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Employee Engagement and Development'",""));

		$promotion = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Promotion (internal and external)'",""));

		$place = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Place (physical and digital)'",""));

		$industry = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Efficiencies'",""));

		$products = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Products and Services'",""));

		$pricing = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Value Based Pricing'",""));

		$investment = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Mindful Investment'",""));

		$userId = get_current_user_id();

		$table_name_answers = $prefix.'questionair_answers';
		$already_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name_answers WHERE `userId` = $userId", ""));


		$all_real_ans = json_decode($already_data[0]->all_real_answers, true);
		$all_exp_ans = json_decode($already_data[0]->all_exp_answers, true);
		
		$total_real_ans = json_decode($already_data[0]->real_answers, true);
		$total_exp_ans = json_decode($already_data[0]->exp_answers, true);

        $real_avg = ($total_real_ans[0] + $total_real_ans[1] + $total_real_ans[2] + $total_real_ans[3] + $total_real_ans[4] + $total_real_ans[5] + 
                    $total_real_ans[6] + $total_real_ans[7] + $total_real_ans[8] + $total_real_ans[9] + $total_real_ans[10] + $total_real_ans[11])/10;
        $real_percent = $real_avg  * 100;
        
        $exp_avg = ($total_exp_ans[0] + $total_exp_ans[1] + $total_exp_ans[2] + $total_exp_ans[3] + $total_exp_ans[4] + $total_exp_ans[5] + 
                    $total_exp_ans[6] + $total_exp_ans[7] + $total_exp_ans[8] + $total_exp_ans[9] + $total_exp_ans[10] + $total_exp_ans[11])/10;
        $exp_percent = $exp_avg  * 100;

		$imgData = str_replace(' ','+',$_POST['imgData']);
		$imgData =  substr($imgData,strpos($imgData,",")+1);
		$imgData = base64_decode($imgData);
		// Path where the image is going to be saved
		$filePath = QUESTIONAIR_PATH . 'assets/temp2.jpeg';
		// Write $imgData into the image file
		$file = fopen($filePath, 'w');
		fwrite($file, $imgData);
		fclose($file);
		ob_start();
		$img = '
		<page><img src="https://mindfulcollective.net/wp-content/uploads/2018/10/themindfulcollectivecover-blue.png" style="float: left; height: 60px"/>
		<h1></h1>
		<br><br>
		<p>Thank you for completing the Mindful Index beta. Your score is :</p>
		<h4>Expectation: '. $exp_percent .'</h4>
		<h4>Reality: '. $real_percent .'</h4>
		
		<p>The Mindful Wheel gives you a visual representation of where you currently are (Realistic) and where you want to be on your 
		mindful journey (Expectation). This allows you to focus on elements of your organisation that need improvement and create a development 
		plan to improve your mindful score. You can view extra resources to help with each element of the mindful journey on our articles page 
		and if you need help in defining and delivering your project plans, contact http://www.themindfulconsultants.com for support.</p><br />
		
		<div style="text-align: center; align-self: center; align-item: center; align-content: center;">';
		if($_POST['resolution'] == "small"){
		    $img.='<img src="'.QUESTIONAIR_PATH.'assets/temp2.jpeg" align="center" style="text-align: center; align-item: center; align-self: center; width: 525px;" alt="image" /></div>';    
		}else{
		    $img.='<img src="'.QUESTIONAIR_PATH.'assets/temp2.jpeg" align="center" style="text-align: center; align-item: center; align-self: center; width: 650px;" alt="image" /></div>';
		}
		
		$content = $img.'</page><page><h2 style="text-align: center;">Purpose</h2><br>
				<table style="text-align: center; border: solid 1px black; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$highest[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($highest as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[0][$i].'</td>
			                <td>'.$all_real_ans[0][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$planet[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($planet as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                
			                <td>'.$all_real_ans[1][$i].'</td>
			                <td>'.$all_exp_ans[1][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$society[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($society as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                
			                <td>'.$all_real_ans[2][$i].'</td>
			                <td>'.$all_exp_ans[2][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table></page>';

		$content.='<page><h2 style="text-align: center;">People</h2><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$customer[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($customer as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[3][$i].'</td>
			                <td>'.$all_real_ans[3][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$leadership[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($leadership as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[4][$i].'</td>
			                <td>'.$all_real_ans[4][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$employee[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($employee as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[5][$i].'</td>
			                <td>'.$all_real_ans[5][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table></page>';

		$content.='<page><h2 style="text-align: center;">Process</h2><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$promotion[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($promotion as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[6][$i].'</td>
			                <td>'.$all_real_ans[6][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$place[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($place as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[7][$i].'</td>
			                <td>'.$all_real_ans[7][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$industry[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($industry as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[8][$i].'</td>
			                <td>'.$all_real_ans[8][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table></page>';

		$content.='<page><h2 style="text-align: center;">Profit</h2><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$products[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($products as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[9][$i].'</td>
			                <td>'.$all_real_ans[9][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$pricing[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($pricing as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[10][$i].'</td>
			                <td>'.$all_real_ans[10][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table>';

		$content.='<br><br>
				<table style="text-align: center; border: solid 2px red; background: #FFEEEE;width: 100%" align="center" border="1">
				    <tr>
				    	<td colspan="3"><b>'.$investment[0]->sub_type.'</b></td>
				    </tr>
				    <tr>
				        <td style="width: 80%">Question</td>
				        <td>Expectation</td>
				        <td>Reality</td>
				    </tr>';
				    $i=0;
			        foreach ($investment as $key => $value) { 
			            $content.='<tr>
			                <td style="width: 80%">'.$value->question.'</td>
			                <td>'.$all_exp_ans[11][$i].'</td>
			                <td>'.$all_real_ans[11][$i].'</td>
			            </tr>';
			            $i++;
			        }
				$content.='</table></page>';

        //header("Content-Disposition: attachment; filename=Mindful.pdf");
        //header('Content-type: application/x-please-download-me');

		try {
			$html2pdf = new Html2Pdf('P', 'A4', 'fr');
			$html2pdf->setDefaultFont("comfortaa");
			$html2pdf->writeHTML($content);
			unlink(QUESTIONAIR_PATH.'assets/temp2.jpeg');
			unlink(QUESTIONAIR_PATH.'assets/Mindful.pdf');
			$html2pdf->output(QUESTIONAIR_PATH.'assets/Mindful.pdf', 'F');
			//$html2pdf->output('Mindful.pdf');
			echo 'done';exit;
		} catch (Html2PdfException $e) {
			$html2pdf->clean();
			$formatter = new ExceptionFormatter($e);
			echo $formatter->getHtmlMessage();
		}

	}
	
	public function reset_all_answers_graph(){
	    global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'questionair_answers';
		$userId = get_current_user_id();
	    $wpdb->query( 'DELETE  FROM '.$table_name.' WHERE userId = "'.$userId.'"');  
	    die('done');
	}
	
	public function questionair_proceed_to_survey(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'questionair_answers';
		$userId = get_current_user_id();
		$check = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `userId` = $userId",""));
		if(count($check) > 0){
		   $wpdb->update( 
				$table_name, 
				array( 'content_viewed' => "Yes" ), 
				array( 'userId' => $userId ), 
				array( '%s' ), 
				array( '%d' ) 
			);
		}else{
		    $wpdb->insert( 
				$table_name, 
				array( 
					'userId' => $userId, 
					'content_viewed' => "Yes"
				), 
				array( 
					'%d', 
					'%s'
				) 
			);
		}
		
		die("done");
	}
}

$Questionair_chart_visualization = new Questionair_chart_visualization();

function questionair_add_page_template ($templates) {
    $templates['show_questions_new.php'] = 'Questionair Template';
    return $templates;
}
add_filter ('theme_page_templates', 'questionair_add_page_template');

function questionair_redirect_page_template ($template) {
    $post = get_post(); 
    $page_template = get_post_meta( $post->ID, '_wp_page_template', true );
    if ('show_questions_new.php' == $page_template)
        $template = QUESTIONAIR_PATH . 'templates/show_questions_new.php';
        return $template;
    }
add_filter ('page_template', 'questionair_redirect_page_template');



function show_frontend_graph_questions() { 
	ob_start(); 
	require_once QUESTIONAIR_PATH.'templates/show_questions.php';
	return ob_get_clean();
}

add_shortcode('show_questionair_graph', 'show_frontend_graph_questions');


function activate_phlox_plugin(){
	create_questionair_table();
	create_questionair_answers_table();		
	insert_questions();
}
register_activation_hook( __FILE__, 'activate_phlox_plugin' );

function create_questionair_table(){
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	global $wpdb;
	$prefix = $wpdb->prefix;
	$table_name = $prefix.'questionair';
	if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
		$create_table_query = "CREATE TABLE `".$table_name."` (
				  `question_id` int(25) NOT NULL AUTO_INCREMENT,
				  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `sub_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `question` text COLLATE utf8mb4_unicode_ci,
				  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  PRIMARY KEY (`question_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci"; 

		dbDelta($create_table_query);
	}
}

function insert_questions(){
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	global $wpdb;
	$prefix = $wpdb->prefix;
	$table_name = $prefix.'questionair';

	$wpdb->query('TRUNCATE TABLE '.$table_name);

	$wpdb->query("INSERT INTO `".$table_name."` (`type`, `sub_type`, `question`, `slug`) VALUES
		('Purpose', 'Highest Purpose and Vision', 'Clear higher purpose, above and beyond the basic need to make profit', 'highest1'),
		('Purpose', 'Highest Purpose and Vision', 'Your higher purpose is communicated clearly throughout your business', 'highest2'),
		('Purpose', 'Highest Purpose and Vision', 'Stakeholders outside of your business know your higher purpose', 'highest3'),
		('Purpose', 'Highest Purpose and Vision', 'You have a clear vision statement', 'highest4'),
		('Purpose', 'Highest Purpose and Vision', 'Your vision statement is understood by everyone in your business', 'highest5'),
		('Purpose', 'Highest Purpose and Vision', 'You have a clear mission statement', 'highest6'),
		('Purpose', 'Highest Purpose and Vision', 'Everyone in your business understands your mission statement', 'highest7'),
		('Purpose', 'Highest Purpose and Vision', 'Your higher purpose is an integral part of your 3-5 year business strategy', 'highest8'),
		('Purpose', 'Highest Purpose and Vision', 'Beyond just stating it, you consistently monitor progress towards achieving your higher purpose', 'highest9'),
		('Purpose', 'Highest Purpose and Vision', 'Throughout your business you have an ethos of good ethics and sustainability', 'highest10'),
		('Purpose', 'Impact on the Planet', 'You make significant use of renewable energy in your business', 'planet1'),
		('Purpose', 'Impact on the Planet', 'Your business gives back at least as much as it takes from the environment', 'planet2'),
		('Purpose', 'Impact on the Planet', 'Environmental Impact Assessment has been conducted in the last 12 months', 'planet3'),
		('Purpose', 'Impact on the Planet', 'You have processes to reduce your impact on the environment', 'planet4'),
		('Purpose', 'Impact on the Planet', 'Your business includes environmental impact in key decision-making', 'planet5'),
		('Purpose', 'Impact on the Planet', 'The business is carbon neutral', 'planet6'),
		('Purpose', 'Impact on the Planet', 'You have a plan across the business, to become or to continue being, carbon neutral', 'planet7'),
		('Purpose', 'Impact on the Planet', 'Your business does it all it can to reduce environmental impact', 'planet8'),
		('Purpose', 'Impact on the Planet', 'When you strategically plan the next 3-5 years you include environmental impact', 'planet9'),
		('Purpose', 'Impact on the Planet', 'There is encouragement for digital communications over physical travel, for example, for meetings', 'planet10'),
		('Purpose', 'Impact on Society', 'Your business can demonstrate a positive effect on the sector in which you operate', 'society1'),
		('Purpose', 'Impact on Society', 'You are making a positive impact in the local community', 'society2'),
		('Purpose', 'Impact on Society', 'When you make decisions as a business you consider the impact on society', 'society3'),
		('Purpose', 'Impact on Society', 'Social Impact Assessment  has been conducted in the last 12 months', 'society4'),
		('Purpose', 'Impact on Society', 'You are making a positive impact on all stakeholders in your supply chain', 'society5'),
		('Purpose', 'Impact on Society', 'Your business carefully considers all stakeholders when strategically planning the next 3-5 years', 'society6'),
		('Purpose', 'Impact on Society', 'You actively engage in appropriate activities in your local community', 'society7'),
		('Purpose', 'Impact on Society', 'There is a clearly communicated plan for reducing negative impacts in the local community, for example, noise and waste', 'society8'),
		('Purpose', 'Impact on Society', 'You consistently consider the next generation when you make decisions as a business', 'society9'),
		('Purpose', 'Impact on Society', 'Your business has an established policy against fraud, corruption and bribery', 'society10'),
		('Mindful People', 'Customer and Community', 'Your strategy, marketing and communications focus on appropriate customer personas', 'customer1'),
		('Mindful People', 'Customer and Community', 'You actively engage in networks and communities where you will find your target personas', 'customer2'),
		('Mindful People', 'Customer and Community', 'You have transparent interactions with all stakeholders and they really ‘know’ you and your team', 'customer3'),
		('Mindful People', 'Customer and Community', 'A Strategic People Plan is in operation throughout your business, covering individuals and teams', 'customer4'),
		('Mindful People', 'Customer and Community', 'You have ethical versions of your products and services, for your ethically-focused customers', 'customer5'),
		('Mindful People', 'Customer and Community', 'You align the higher purpose of your business with your customers’ needs', 'customer6'),
		('Mindful People', 'Customer and Community', 'There are established processes for capturing and acting on feedback from your stakeholders', 'customer7'),
		('Mindful People', 'Customer and Community', 'You take a positive approach to the ‘gig economy’', 'customer8'),
		('Mindful People', 'Customer and Community', 'Your staff are able to choose flexible working, aligning their needs with the needs of the business', 'customer9'),
		('Mindful People', 'Customer and Community', 'You have evidence of putting the customer first, throughout your business', 'customer10'),
		('Mindful People', 'Leadership and Culture', 'Your leadership proactively supports the higher purpose, vision and mission', 'leadership1'),
		('Mindful People', 'Leadership and Culture', 'Business leaders proactively encourage everyone to deliver technical innovation', 'leadership2'),
		('Mindful People', 'Leadership and Culture', 'Leaders proactively encourage innovation in culture and team working across the business', 'leadership3'),
		('Mindful People', 'Leadership and Culture', 'A Diversity and Inclusion Plan is in operation throughout the business', 'leadership4'),
		('Mindful People', 'Leadership and Culture', 'Leaders are empowered and think and operate using both their heart and mind', 'leadership5'),
		('Mindful People', 'Leadership and Culture', 'Leaders know their people and actively engage with them on an appropriate and regular basis', 'leadership6'),
		('Mindful People', 'Leadership and Culture', 'There is a mindful culture throughout the business', 'leadership7'),
		('Mindful People', 'Leadership and Culture', 'Leaders are compensated for delivering sustainable and ethical objectives', 'leadership8'),
		('Mindful People', 'Leadership and Culture', 'The business culture is progressive in social, economic and environmental responsibility', 'leadership9'),
		('Mindful People', 'Leadership and Culture', 'Leadership proactively encourages diversity, inclusivity and progression', 'leadership10'),
		('Mindful People', 'Employee Engagement and Development', 'We consistently monitor and respond to employee satisfaction', 'employee1'),
		('Mindful People', 'Employee Engagement and Development', 'Our employee values are aligned with the higher purpose of the business', 'employee2'),
		('Mindful People', 'Employee Engagement and Development', 'When we recruit we clearly communicate our higher purpose and business culture', 'employee3'),
		('Mindful People', 'Employee Engagement and Development', 'We have a 360 degree review processes across the business', 'employee4'),
		('Mindful People', 'Employee Engagement and Development', 'Everyone in the business has a personal development plan', 'employee5'),
		('Mindful People', 'Employee Engagement and Development', 'We have a continuous improvement plan focusing on employee engagement and loyalty', 'employee6'),
		('Mindful People', 'Employee Engagement and Development', 'Team development plans support our higher purpose', 'employee7'),
		('Mindful People', 'Employee Engagement and Development', 'Remuneration and compensation is at least the average in our sector', 'employee8'),
		('Mindful People', 'Employee Engagement and Development', 'Employee Wellness Programme including fitness, mental health and healthcare', 'employee9'),
		('Mindful People', 'Employee Engagement and Development', 'We celebrate success across the business', 'employee10'),
		('Mindful Process', 'Promotion (internal and external)', 'We keep everyone informed in our business to support employee satisfaction, engagement and loyalty', 'promotion1'),
		('Mindful Process', 'Promotion (internal and external)', 'Our marketing and communication is authentic, honest and truthful', 'promotion2'),
		('Mindful Process', 'Promotion (internal and external)', 'Wherever we can, we communicate transparently across our business', 'promotion3'),
		('Mindful Process', 'Promotion (internal and external)', 'There is evidence of our integrity throughout our business', 'promotion4'),
		('Mindful Process', 'Promotion (internal and external)', 'When we communicate, we do so, with the best interests of all stakeholders, in mind', 'promotion5'),
		('Mindful Process', 'Promotion (internal and external)', 'When we target our customers with marketing and communications, we do so ethically', 'promotion6'),
		('Mindful Process', 'Promotion (internal and external)', 'We collect data to benefit all stakeholders and enable us to personalize the customer experience', 'promotion7'),
		('Mindful Process', 'Promotion (internal and external)', 'Higher purpose and our values are consistently communicated', 'promotion8'),
		('Mindful Process', 'Promotion (internal and external)', 'When we collect and hold customer data, we comply with all legislation and standards', 'promotion9'),
		('Mindful Process', 'Promotion (internal and external)', 'Where possible we use digital rather than physical communication to help reduce our carbon footprint', 'promotion10'),
		('Mindful Process', 'Place (physical and digital)', 'We make best use of our physical spaces and are mindful of waste and environmental impact', 'place1'),
		('Mindful Process', 'Place (physical and digital)', 'Everyone strives for maximum efficiency in all processes', 'place2'),
		('Mindful Process', 'Place (physical and digital)', 'We focus to continually improve the efficiency of the physical assets in our supply chain', 'place3'),
		('Mindful Process', 'Place (physical and digital)', 'We select appropriate supply chain partners who share a similar ethos and higher purpose', 'place4'),
		('Mindful Process', 'Place (physical and digital)', 'We have highly efficient logistics and transportation', 'place5'),
		('Mindful Process', 'Place (physical and digital)', 'Our processes include the use of remote-working and home-working', 'place6'),
		('Mindful Process', 'Place (physical and digital)', 'We spend budget wisely throughout all processes in our business', 'place7'),
		('Mindful Process', 'Place (physical and digital)', 'We proactively convert of physical places to digital, wherever possible, to reduce our carbon footprint', 'place8'),
		('Mindful Process', 'Place (physical and digital)', 'There are active energy efficiency and waste reduction programmes across the business', 'place9'),
		('Mindful Process', 'Place (physical and digital)', 'We allow customers to use digital tools to access our products and services to maximize', 'place10'),
		('Mindful Process', 'Efficiencies', 'We make appropriate use of data to make our business more efficient', 'industry1'),
		('Mindful Process', 'Efficiencies', 'We understand and use new technologies to increase efficiencies and in new product development', 'industry2'),
		('Mindful Process', 'Efficiencies', 'We continually plan to integrate and optimize new technologies and processes', 'industry3'),
		('Mindful Process', 'Efficiencies', 'There are ongoing impact assessments of process and technology in our business', 'industry4'),
		('Mindful Process', 'Efficiencies', 'We conduct assessments of the impact of our processes and technologies, on our people', 'industry5'),
		('Mindful Process', 'Efficiencies', 'Our systems and processes are centralized or decentralized in line with our higher purpose', 'industry6'),
		('Mindful Process', 'Efficiencies', 'We use data to bring greater process transparency across our business', 'industry7'),
		('Mindful Process', 'Efficiencies', 'We are continually evaluating how emerging technologies could bring greater process efficiencies', 'industry8'),
		('Mindful Process', 'Efficiencies', 'Company is ready for the integration of industry 4.0 technologies', 'industry9'),
		('Mindful Process', 'Efficiencies', 'Highest purpose and evidence based customer needs are integral to new product development', 'industry10'),
		('Mindful Profit', 'Products and Services', 'Our products profitably support our higher purpose as well as the profit targets we set', 'products1'),
		('Mindful Profit', 'Products and Services', 'The services we offer support not only our profit but our higher purpose', 'products2'),
		('Mindful Profit', 'Products and Services', 'Our products are developed and delivered ethically along our entire supply chain', 'products3'),
		('Mindful Profit', 'Products and Services', 'The services we offer are mindful, ethical and in line with our higher purpose', 'products4'),
		('Mindful Profit', 'Products and Services', 'Where we seek to reduce the cost of our products, we do so ethically', 'products5'),
		('Mindful Profit', 'Products and Services', 'If we reduce the cost of our services we do so mindful of the impact on our stakeholders and supply chain', 'products6'),
		('Mindful Profit', 'Products and Services', 'Where we reduce cost in our processes, we do so ethically and sustainably', 'products7'),
		('Mindful Profit', 'Products and Services', 'All of our products are produced ethically, without unnecessary wastage or harm to the environment', 'products8'),
		('Mindful Profit', 'Products and Services', 'All of our services are delivered ethically and sustainably', 'products9'),
		('Mindful Profit', 'Products and Services', 'We constantly monitor and seek to improve the impact of our product production', 'products10'),
		('Mindful Profit', 'Value Based Pricing', 'Our pricing strategy is based on customers’ perceived value of products and services', 'pricing1'),
		('Mindful Profit', 'Value Based Pricing', 'We can show evidence of the tangible benefits our customers from our products and services', 'pricing2'),
		('Mindful Profit', 'Value Based Pricing', 'We listen to our customers and their needs and best interests, drive our pricing strategy', 'pricing3'),
		('Mindful Profit', 'Value Based Pricing', 'There is ongoing collection and positive use of customer feedback throughout the business', 'pricing4'),
		('Mindful Profit', 'Value Based Pricing', 'We constantly review and respond to customer satisfaction measures and feedback', 'pricing5'),
		('Mindful Profit', 'Value Based Pricing', 'Our value-based pricing is at least as appropriate as the average in our sector', 'pricing6'),
		('Mindful Profit', 'Value Based Pricing', 'We demonstrate the customers’ return on their investment in our products', 'pricing7'),
		('Mindful Profit', 'Value Based Pricing', 'Our customers tell us they receive positive return on their investment in our services', 'pricing8'),
		('Mindful Profit', 'Value Based Pricing', 'Pricing is managed and monitored by appropriate people in the business', 'pricing9'),
		('Mindful Profit', 'Value Based Pricing', 'Pricing strategy is aligned to the higher purpose of the business', 'pricing10'),
		('Mindful Profit', 'Mindful Investment', 'The accounting practice and reporting in our business is ethical and mindful and in line with our higher purpose', 'investment1'),
		('Mindful Profit', 'Mindful Investment', 'We practice mindful tax administration and reporting', 'investment2'),
		('Mindful Profit', 'Mindful Investment', 'We practice mindful pension administration and reporting', 'investment3'),
		('Mindful Profit', 'Mindful Investment', 'Our business is transparency in how and when we pay our corporation tax commitments', 'investment4'),
		('Mindful Profit', 'Mindful Investment', 'Our re-investment in assets is in line with our higher purpose, vision and mission at all times', 'investment5'),
		('Mindful Profit', 'Mindful Investment', 'Where appropriate, we consistently demonstrate ethical distribution of all profits', 'investment6'),
		('Mindful Profit', 'Mindful Investment', 'An appropriate level of our profits are reinvested into research, innovation and people development', 'investment7'),
		('Mindful Profit', 'Mindful Investment', 'Mindful financial practice is consistent across our business and improvements are sought at every level', 'investment8'),
		('Mindful Profit', 'Mindful Investment', 'The remuneration gap between CEO, leadership and our lowest paid staff, is fair and justified', 'investment9'),
		('Mindful Profit', 'Mindful Investment', 'We clearly communicate to our stakeholders where profits are re-invested', 'investment10')");
}

function create_questionair_answers_table(){
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	global $wpdb;
	$prefix = $wpdb->prefix;
	$table_name = $prefix.'questionair_answers';
	$wpdb->query('DROP TABLE "$table_name"');  
	if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
		$create_table_query = "CREATE TABLE `".$table_name."` (
				  `answer_id` int(25) NOT NULL AUTO_INCREMENT,
				  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `exp_answers` text COLLATE utf8mb4_unicode_ci,
				  `real_answers` text COLLATE utf8mb4_unicode_ci,
				  `all_real_answers` text COLLATE utf8mb4_unicode_ci,
				  `all_exp_answers` text COLLATE utf8mb4_unicode_ci,
				  `content_viewed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
				  PRIMARY KEY (`answer_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci"; 

		dbDelta($create_table_query);
	}
}