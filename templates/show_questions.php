<?php
if (is_user_logged_in()) {

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    global $wpdb;
    $prefix = $wpdb->prefix;
    $table_name = $prefix.'questionair';
    
    $highest = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Highest Purpose and Vision'",""));
    $planet = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Impact on the Planet'",""));
    $society = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Impact on Society'",""));
    $customer = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Customer and Community'",""));
    $leadership = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Leadership and Culture'",""));
    $employee = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Employee Engagement and Development'",""));
    $promotion = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Promotion (internal and external)'",""));
    $place = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Place (physical and digital)'",""));
    $industry = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Efficiencies'",""));
    $products = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Products and Services'",""));
    $pricing = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Mindful Pricing'",""));
    $investment = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Mindful Investment'",""));
    
    $userId = get_current_user_id();
    
    $table_name_answers = $prefix.'questionair_answers';
    $already_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name_answers WHERE `userId` = $userId", ""));
    
    $all_real_ans = json_decode($already_data[0]->all_real_answers, true);
    $all_exp_ans = json_decode($already_data[0]->all_exp_answers, true);
    
    $all_arr_exp = json_decode($already_data[0]->exp_answers, true);
    $all_arr_real = json_decode($already_data[0]->real_answers, true);
    
    $progres_count = 0;
    foreach ($all_arr_real as $key => $value) {
        if($value > 0){
            $progres_count++;
        }
    }
    $progres = ($progres_count*100)/12;

?>
    <style type="text/css">
        @media (min-width: 960px) { .limit-width { max-width: none !important; margin: auto;}}
        footer{
            display: none !important;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <div id="show_msg"></div>
    <div class="row">
        <div style="width: 25%; float: left; text-align: center; border-right: 1px solid;">Purpose</div>
        <div style="width: 25%; float: left; text-align: center; border-right: 1px solid;">People</div>
        <div style="width: 25%; float: left; text-align: center; border-right: 1px solid;">Process</div>
        <div style="width: 25%; float: left; text-align: center;">Profit</div>
    </div>
    <div class="progress">
      <div class="progress-bar progress-bar-striped bg-info active" role="progressbar" style="width: <?php echo $progres ?>%" aria-valuenow="<?php echo $progres ?>" aria-valuemin="0" aria-valuemax="100">
      </div>
    </div>
    <br><br>

    <div class="container containers-graph" style="width: 1200px;">
        <div id="body" style="width: 540px; float: left;">
            <div id="Questionairchart" style="background: url('<?php echo QUESTIONAIR_URL ?>assets/document.png') no-repeat; background-size: contain;"></div>
        </div>
          
        <div class="questionDiv highest" style="display: <?php echo ($progres_count == 0) ? 'block' : 'none'; ?>">
          <center><h4>Highest Purpose and Vision</h4></center>
            <?php 
            $i = 1;
            foreach ($highest as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-highest<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-highest<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-highest<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-highest<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('highest', 'planet')">Next</a>
        </div>
    
        <div class="questionDiv planet" style="display: <?php echo ($progres_count == 1) ? 'block' : 'none'; ?>">
          <center><h4>Impact on the Planet</h4></center>
            <?php 
            $i = 1;
            foreach ($planet as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-planet<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-planet<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-planet<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-planet<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('highest', 'planet')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('planet', 'society')">Next</a>
        </div>
    
        <div class="questionDiv society" style="display: <?php echo ($progres_count == 2) ? 'block' : 'none'; ?>">
            <center><h4>Impact on Society</h4></center>
            <?php 
            $i = 1;
            foreach ($society as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-society<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-society<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-society<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-society<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('planet', 'society')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('society', 'customer')">Next</a>
        </div>
    
        <div class="questionDiv customer" style="display: <?php echo ($progres_count == 3) ? 'block' : 'none'; ?>">
          <center><h4>Customer and Community</h4></center>
            <?php 
            $i = 1;
            foreach ($customer as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-customer<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-customer<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-customer<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-customer<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('society', 'customer')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('customer', 'leadership')">Next</a>
        </div>
    
        <div class="questionDiv leadership" style="display: <?php echo ($progres_count == 4) ? 'block' : 'none'; ?>">
          <center><h4>Leadership and Culture</h4></center>
            <?php 
            $i = 1;
            foreach ($leadership as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-leadership<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-leadership<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-leadership<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-leadership<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('customer', 'leadership')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('leadership', 'employee')">Next</a>
        </div>
    
        <div class="questionDiv employee" style="display: <?php echo ($progres_count == 5) ? 'block' : 'none'; ?>">
          <center><h4>Employee Engagement and Development</h4></center>
            <?php 
            $i = 1;
            foreach ($employee as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-employee<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-employee<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-employee<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-employee<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('leadership', 'employee')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('employee', 'promotion')">Next</a>
        </div>   
    
        <div class="questionDiv promotion" style="display: <?php echo ($progres_count == 6) ? 'block' : 'none'; ?>">
          <center><h4>Promotion (internal and external)</h4></center>
            <?php 
            $i = 1;
            foreach ($promotion as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-promotion<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-promotion<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-promotion<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-promotion<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('employee', 'promotion')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('promotion', 'place')">Next</a>
        </div>  
    
        <div class="questionDiv place" style="display: <?php echo ($progres_count == 7) ? 'block' : 'none'; ?>">
          <center><h4>Place (physical and digital)</h4></center>
            <?php 
            $i = 1;
            foreach ($place as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-place<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-place<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-place<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-place<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('promotion', 'place')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('place', 'industry')">Next</a>
        </div>
    
        <div class="questionDiv industry" style="display: <?php echo ($progres_count == 8) ? 'block' : 'none'; ?>">
          <center><h4>Efficiencies</h4></center>
            <?php 
            $i = 1;
            foreach ($industry as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-industry<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-industry<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-industry<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-industry<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('place', 'industry')">Previous</a>
            <a  class="btn btn-primary nxtBtn" onclick="showNextQuestions('industry', 'products')">Next</a>
        </div> 
    
        <div class="questionDiv products" style="display: <?php echo ($progres_count == 9) ? 'block' : 'none'; ?>">
          <center><h4>Products and Services</h4></center>
            <?php 
            $i = 1;
            foreach ($products as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-products<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-products<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-products<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-products<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('industry', 'products')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('products', 'pricing')">Next</a>
        </div>
    
        <div class="questionDiv pricing" style="display: <?php echo ($progres_count == 10) ? 'block' : 'none'; ?>">
          <center><h4>Mindful Pricing</h4></center>
            <?php 
            $i = 1;
            foreach ($pricing as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-pricing<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-pricing<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-pricing<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-pricing<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('products', 'pricing')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="showNextQuestions('pricing', 'investment')">Next</a>
        </div> 
    
        <div class="questionDiv investment" style="display: <?php echo ($progres_count == 11 || $progres_count == 12) ? 'block' : 'none'; ?>">
          <center><h4>Mindful Investment</h4></center>
            <?php 
            $i = 1;
            foreach ($investment as $key => $value) {?>
                <div>
                    <label><?php echo $value->question ?></label><br>
                    Expectation: <input type="text" id="expectation-investment<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-expectation-investment<?php echo $i ?>"></div>
    
                    Reality: <input type="text" id="reality-investment<?php echo $i ?>" class="rangeInput" readonly>
                    <div id="slider-reality-investment<?php echo $i ?>"></div>
                </div>
                <hr class="hrCss">
            <?php $i++;
            } ?>
            <a class="btn btn-primary prevBtn" onclick="showPreviouQuestions('pricing', 'investment')">Previous</a>
            <a class="btn btn-primary nxtBtn" onclick="updateData()">Finish</a>
        </div> 
    </div> 
    <h2 class="toCanvas pull-right"> <a class="btn btn-danger">Download PDF</a></h2>
    <h2 class="pull-right resetGraphAll" style="margin-right: 10px;"> <a class="btn btn-danger">Reset</a></h2>

    <br>
    <div>Powered By <img src="<?php echo QUESTIONAIR_URL ?>assets/poweredby.png" style="width: 150px;"></div>


    <div class="ajax-loader" style="display:none;">
        <div class="image-laoding">
            <img src="<?php echo QUESTIONAIR_URL ?>assets/loader.gif" title="processing..." alt="loading..."/>
        </div>
        <div class="background-loader"></div>
    </div>

    <script>
        $( function() {
            <?php 
            $i = 1;
            foreach ($highest as $key => $value) {?>
                $("#slider-expectation-highest<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[0][$i-1]) ? $all_exp_ans[0][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-highest<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-highest<?php echo $i; ?>").val($("#slider-expectation-highest<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-highest<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[0][$i-1]) ? $all_real_ans[0][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-highest<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-highest<?php echo $i; ?>").val($("#slider-reality-highest<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($planet as $key => $value) {?>
                $("#slider-expectation-planet<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[2][$i-1]) ? $all_exp_ans[2][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-planet<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-planet<?php echo $i; ?>").val($("#slider-expectation-planet<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-planet<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[2][$i-1]) ? $all_real_ans[2][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-planet<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-planet<?php echo $i; ?>").val($("#slider-reality-planet<?php echo $i; ?>").slider("value"));
    
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($society as $key => $value) {?>
                $("#slider-expectation-society<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[1][$i-1]) ? $all_exp_ans[1][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-society<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-society<?php echo $i; ?>").val($("#slider-expectation-society<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-society<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[1][$i-1]) ? $all_real_ans[1][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-society<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-society<?php echo $i; ?>").val($("#slider-reality-society<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($customer as $key => $value) {?>
                $("#slider-expectation-customer<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[3][$i-1]) ? $all_exp_ans[3][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-customer<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-customer<?php echo $i; ?>").val($("#slider-expectation-customer<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-customer<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[3][$i-1]) ? $all_real_ans[3][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-customer<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-customer<?php echo $i; ?>").val($("#slider-reality-customer<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($leadership as $key => $value) {?>
                $("#slider-expectation-leadership<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[4][$i-1]) ? $all_exp_ans[4][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-leadership<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-leadership<?php echo $i; ?>").val($("#slider-expectation-leadership<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-leadership<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[4][$i-1]) ? $all_real_ans[4][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-leadership<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-leadership<?php echo $i; ?>").val($("#slider-reality-leadership<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($employee as $key => $value) {?>
                $("#slider-expectation-employee<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[5][$i-1]) ? $all_exp_ans[5][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-employee<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-employee<?php echo $i; ?>").val($("#slider-expectation-employee<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-employee<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[5][$i-1]) ? $all_real_ans[5][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-employee<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-employee<?php echo $i; ?>").val($("#slider-reality-employee<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($promotion as $key => $value) {?>
                $("#slider-expectation-promotion<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[6][$i-1]) ? $all_exp_ans[6][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-promotion<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-promotion<?php echo $i; ?>").val($("#slider-expectation-promotion<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-promotion<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[6][$i-1]) ? $all_real_ans[6][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-promotion<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-promotion<?php echo $i; ?>").val($("#slider-reality-promotion<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($place as $key => $value) {?>
                $("#slider-expectation-place<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[7][$i-1]) ? $all_exp_ans[7][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-place<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-place<?php echo $i; ?>").val($("#slider-expectation-place<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-place<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[7][$i-1]) ? $all_real_ans[7][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-place<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-place<?php echo $i; ?>").val($("#slider-reality-place<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($industry as $key => $value) {?>
                $("#slider-expectation-industry<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[8][$i-1]) ? $all_exp_ans[8][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-industry<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-industry<?php echo $i; ?>").val($("#slider-expectation-industry<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-industry<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[8][$i-1]) ? $all_real_ans[8][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-industry<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-industry<?php echo $i; ?>").val($("#slider-reality-industry<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($products as $key => $value) {?>
                $("#slider-expectation-products<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[9][$i-1]) ? $all_exp_ans[9][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-products<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-products<?php echo $i; ?>").val($("#slider-expectation-products<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-products<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[9][$i-1]) ? $all_real_ans[9][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-products<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-products<?php echo $i; ?>").val($("#slider-reality-products<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($pricing as $key => $value) {?>
                $("#slider-expectation-pricing<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[10][$i-1]) ? $all_exp_ans[10][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-pricing<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-pricing<?php echo $i; ?>").val($("#slider-expectation-pricing<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-pricing<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[10][$i-1]) ? $all_real_ans[10][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-pricing<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-pricing<?php echo $i; ?>").val($("#slider-reality-pricing<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
    
            <?php 
            $i = 1;
            foreach ($investment as $key => $value) {?>
                $("#slider-expectation-investment<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_exp_ans[11][$i-1]) ? $all_exp_ans[11][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#expectation-investment<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#expectation-investment<?php echo $i; ?>").val($("#slider-expectation-investment<?php echo $i; ?>").slider("value"));
    
                $("#slider-reality-investment<?php echo $i; ?>").slider({
                    range: "max",
                    min: 0,
                    max: 10,
                    value: <?php echo isset($all_real_ans[11][$i-1]) ? $all_real_ans[11][$i-1] : 0  ?>,
                    slide: function(event, ui) {
                        $("#reality-investment<?php echo $i; ?>").val(ui.value);
                        updateGraph();
                    }
                });
                $("#reality-investment<?php echo $i; ?>").val($("#slider-reality-investment<?php echo $i; ?>").slider("value"));
            <?php $i++;
            } ?>
        });
    
        function updateGraph(){
    
            var allArrExp = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
            allArrExp.forEach(function(item, index, arr){
                for (var i = 10 - 1; i >= 0; i--) {
                    var q1 = $("#expectation-" + item + "1").val();
                    q2 = $("#expectation-" + item + "2").val(),
                    q3 = $("#expectation-" + item + "3").val(),
                    q4 = $("#expectation-" + item + "4").val(),
                    q5 = $("#expectation-" + item + "5").val(),
                    q6 = $("#expectation-" + item + "6").val(),
                    q7 = $("#expectation-" + item + "7").val(),
                    q8 = $("#expectation-" + item + "8").val(),
                    q9 = $("#expectation-" + item + "9").val(),
                    q10 = $("#expectation-" + item + "10").val();
                    
                }
                arr[index] = (parseInt(q1) + parseInt(q2) + parseInt(q3) + parseInt(q4) + parseInt(q5) + parseInt(q6) + parseInt(q7) + parseInt(q8) + parseInt(q9) + parseInt(q10))/100;
            });
    
            var allArrReal = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
            allArrReal.forEach(function(item, index, arr){
                for (var i = 10 - 1; i >= 0; i--) {
                    var q1 = $("#reality-" + item + "1").val();
                    q2 = $("#reality-" + item + "2").val(),
                    q3 = $("#reality-" + item + "3").val(),
                    q4 = $("#reality-" + item + "4").val(),
                    q5 = $("#reality-" + item + "5").val(),
                    q6 = $("#reality-" + item + "6").val(),
                    q7 = $("#reality-" + item + "7").val(),
                    q8 = $("#reality-" + item + "8").val(),
                    q9 = $("#reality-" + item + "9").val(),
                    q10 = $("#reality-" + item + "10").val();
                    
                }
                arr[index] = (parseInt(q1) + parseInt(q2) + parseInt(q3) + parseInt(q4) + parseInt(q5) + parseInt(q6) + parseInt(q7) + parseInt(q8) + parseInt(q9) + parseInt(q10))/100;
            });
    
            
            if($(window).width() < 1199 && $(window).width() > 991){
                var w = 450, h = 450;
            }else if($(window).width() < 992 && $(window).width() > 885){
                var w = 330, h = 330;    
            }else if($(window).width() < 570 && $(window).width() > 480){
                var w = 340, h = 340;
            }else if($(window).width() < 480){
                var w = 320, h = 320;
            }else{
                var w = 500, h = 500;
            }
            var colorscale = d3.scale.category10();
            var d = [
                [
                    {axis:"",value:allArrExp[3]},  //Customer and community
                    {axis:"",value:allArrExp[1]},  //impact on society
                    {axis:"",value:allArrExp[2]},  //impact on planet
                    {axis:"",value:allArrExp[0]},  //highest purpose
                    {axis:"",value:allArrExp[11]},  //Mindful Investment
                    {axis:"",value:allArrExp[10]},  //Mindful Pricing
                    {axis:"",value:allArrExp[9]},  //Products and Services
                    {axis:"",value:allArrExp[8]},  //Efficiencies(Industry)
                    {axis:"",value:allArrExp[7]},  //Place (physical and digital)
                    {axis:"",value:allArrExp[6]},  //Promotion (internal and external)
                    {axis:"",value:allArrExp[5]},  //Employee Engagement and Development
                    {axis:"",value:allArrExp[4]},  //Leadership and Culture
                ],
                [
                    {axis:"",value:allArrReal[3]},  //Customer and community
                    {axis:"",value:allArrReal[1]},  //impact on society
                    {axis:"",value:allArrReal[2]},  //impact on planet
                    {axis:"",value:allArrReal[0]},  //highest purpose
                    {axis:"",value:allArrReal[11]},  //Mindful Investment
                    {axis:"",value:allArrReal[10]},  //Mindful Pricing
                    {axis:"",value:allArrReal[9]},  //Products and Services
                    {axis:"",value:allArrReal[8]},  //Efficiencies(Industry)
                    {axis:"",value:allArrReal[7]},  //Place (physical and digital)
                    {axis:"",value:allArrReal[6]},  //Promotion (internal and external)
                    {axis:"",value:allArrReal[5]},  //Employee Engagement and Development
                    {axis:"",value:allArrReal[4]},  //Leadership and Culture
                ]
            ];
    
            //Options for the Radar chart, other than default
            var mycfg = {
                w: w,
                h: h,
                maxValue: 1,
                levels: 0,
                ExtraWidthX: 300
            }
            
            RadarChart.draw("#Questionairchart", d, mycfg);
            
        }
       
        // console.log($(window).width());
        if($(window).width() < 1199 && $(window).width() > 991){
            //console.log($(window).width());
            var w = 450, h = 450;
        }else if($(window).width() < 992 && $(window).width() > 885){
            console.log($(window).width());
            var w = 330, h = 330;    
        }else if($(window).width() < 570 && $(window).width() > 480){
            var w = 340, h = 340;
        }else if($(window).width() < 480){
            var w = 350, h = 350;
        }else{
            var w = 500, h = 500;
        }
    
        var colorscale = d3.scale.category10();
    
        var d = [
            [
                {axis:"",value:<?php echo isset($all_arr_exp[3]) ? $all_arr_exp[3] : 0 ?>},  //Customer and community
                {axis:"",value:<?php echo isset($all_arr_exp[1]) ? $all_arr_exp[1] : 0 ?>},  //impact on society
                {axis:"",value:<?php echo isset($all_arr_exp[2]) ? $all_arr_exp[2] : 0 ?>},  //impact on planet
                {axis:"",value:<?php echo isset($all_arr_exp[0]) ? $all_arr_exp[0] : 0 ?>},  //highest purpose
                {axis:"",value:<?php echo isset($all_arr_exp[11]) ? $all_arr_exp[11] : 0 ?>},  //Mindful Investment
                {axis:"",value:<?php echo isset($all_arr_exp[10]) ? $all_arr_exp[10] : 0 ?>},  //Mindful Pricing
                {axis:"",value:<?php echo isset($all_arr_exp[9]) ? $all_arr_exp[9] : 0 ?>},  //Products and Services
                {axis:"",value:<?php echo isset($all_arr_exp[8]) ? $all_arr_exp[8] : 0 ?>},  //Efficiencies(Industry)
                {axis:"",value:<?php echo isset($all_arr_exp[7]) ? $all_arr_exp[7] : 0 ?>},  //Place (physical and digital)
                {axis:"",value:<?php echo isset($all_arr_exp[6]) ? $all_arr_exp[6] : 0 ?>},  //Promotion (internal and external)
                {axis:"",value:<?php echo isset($all_arr_exp[5]) ? $all_arr_exp[5] : 0 ?>},  //Employee Engagement and Development
                {axis:"",value:<?php echo isset($all_arr_exp[4]) ? $all_arr_exp[4] : 0 ?>},  //Leadership and Culture
            ],
            [
                {axis:"",value:<?php echo isset($all_arr_real[3]) ? $all_arr_real[3] : 0 ?>},  //Customer and community
                {axis:"",value:<?php echo isset($all_arr_real[1]) ? $all_arr_real[1] : 0 ?>},  //impact on society
                {axis:"",value:<?php echo isset($all_arr_real[2]) ? $all_arr_real[2] : 0 ?>},  //impact on planet
                {axis:"",value:<?php echo isset($all_arr_real[0]) ? $all_arr_real[0] : 0 ?>},  //highest purpose
                {axis:"",value:<?php echo isset($all_arr_real[11]) ? $all_arr_real[11] : 0 ?>},  //Mindful Investment
                {axis:"",value:<?php echo isset($all_arr_real[10]) ? $all_arr_real[10] : 0 ?>},  //Mindful Pricing
                {axis:"",value:<?php echo isset($all_arr_real[9]) ? $all_arr_real[9] : 0 ?>},  //Products and Services
                {axis:"",value:<?php echo isset($all_arr_real[8]) ? $all_arr_real[8] : 0 ?>},  //Efficiencies(Industry)
                {axis:"",value:<?php echo isset($all_arr_real[7]) ? $all_arr_real[7] : 0 ?>},  //Place (physical and digital)
                {axis:"",value:<?php echo isset($all_arr_real[6]) ? $all_arr_real[6] : 0 ?>},  //Promotion (internal and external)
                {axis:"",value:<?php echo isset($all_arr_real[5]) ? $all_arr_real[5] : 0 ?>},  //Employee Engagement and Development
                {axis:"",value:<?php echo isset($all_arr_real[4]) ? $all_arr_real[4] : 0 ?>},  //Leadership and Culture
            ]
        ];
    
        var mycfg = {
            w: w,
            h: h,
            maxValue: 1,
            levels: 0,
            ExtraWidthX: 300
        }
    
        RadarChart.draw("#Questionairchart", d, mycfg);
    
        function showNextQuestions(now, next){
            updateData();
            $("."+now).fadeOut();
            setTimeout(function(){  
                $("."+next).toggle("slide", { direction: "right" }, 500);
            }, 500);
        }
    
        function showPreviouQuestions(prev, now){
            $("."+now).fadeOut();
            setTimeout(function(){  
                $("."+prev).toggle("slide", { direction: "left" }, 1000);
            }, 500);
        }
    
        function updateData(){
            jQuery('.ajax-loader').show();
            var allArrExp = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
            var allResExp = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
            
            allArrExp.forEach(function(item, index, arr){
                for (var i = 10 - 1; i >= 0; i--) {
                    var q1 = $("#expectation-" + item + "1").val();
                    q2 = $("#expectation-" + item + "2").val(),
                    q3 = $("#expectation-" + item + "3").val(),
                    q4 = $("#expectation-" + item + "4").val(),
                    q5 = $("#expectation-" + item + "5").val(),
                    q6 = $("#expectation-" + item + "6").val(),
                    q7 = $("#expectation-" + item + "7").val(),
                    q8 = $("#expectation-" + item + "8").val(),
                    q9 = $("#expectation-" + item + "9").val(),
                    q10 = $("#expectation-" + item + "10").val();
                    
                }
                arr[index] = (parseInt(q1) + parseInt(q2) + parseInt(q3) + parseInt(q4) + parseInt(q5) + parseInt(q6) + parseInt(q7) + parseInt(q8) + parseInt(q9) + parseInt(q10))/100;
                allResExp[index] = [parseInt(q1), parseInt(q2), parseInt(q3), parseInt(q4), parseInt(q5), parseInt(q6), parseInt(q7), parseInt(q8), parseInt(q9), parseInt(q10)];
            });
    
            var allArrReal = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
            var allResReal = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
            allArrReal.forEach(function(item, index, arr){
                for (var i = 10 - 1; i >= 0; i--) {
                    var q1 = $("#reality-" + item + "1").val();
                    q2 = $("#reality-" + item + "2").val(),
                    q3 = $("#reality-" + item + "3").val(),
                    q4 = $("#reality-" + item + "4").val(),
                    q5 = $("#reality-" + item + "5").val(),
                    q6 = $("#reality-" + item + "6").val(),
                    q7 = $("#reality-" + item + "7").val(),
                    q8 = $("#reality-" + item + "8").val(),
                    q9 = $("#reality-" + item + "9").val(),
                    q10 = $("#reality-" + item + "10").val();
                    
                }
                arr[index] = (parseInt(q1) + parseInt(q2) + parseInt(q3) + parseInt(q4) + parseInt(q5) + parseInt(q6) + parseInt(q7) + parseInt(q8) + parseInt(q9) + parseInt(q10))/100;
                allResReal[index] = [parseInt(q1), parseInt(q2), parseInt(q3), parseInt(q4), parseInt(q5), parseInt(q6), parseInt(q7), parseInt(q8), parseInt(q9), parseInt(q10)];
            });
            
            var progressCount = 0;
            allArrReal.forEach(function(item, index, arr){
                if(item > 0){
                    progressCount++;
                }
            });
            var percent = (progressCount*100)/12;
    
            $(".progress-bar-striped").css("width", percent + "%");
            
            var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
    
            jQuery.ajax({
                type: 'POST',   
                url: ajaxurl, 
                data: {"allArrExp": allArrExp, "allArrReal": allArrReal, "allResExp": allResExp, "allResReal": allResReal, "action": "save_all_questionair_result"}, 
                success: function(data) {
                    jQuery('.ajax-loader').hide();
                    if (data == 'done') {
                        /*jQuery("#show_msg").html('<div class="updated notice is-dismissible"><p>Successfully Saved!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div><br>');
                        jQuery("html").scrollTop(0);
                        setTimeout(function(){ 
                            jQuery("#show_msg").html(""); 
                        }, 7000);*/
                        //alert('Successfully Updated');
                    }else{
                        alert('Errorrr');
                    }
                }
            });
        }
    
        var test = $("#Questionairchart").get(0);
        
        $('.toCanvas').click(function(e) {
            jQuery('.ajax-loader').show();
            html2canvas(test).then(function(canvas) {
                var canvasWidth = canvas.width;
                var canvasHeight = canvas.height;
                
                $('.toPic').show(1000);
                var img = Canvas2Image.convertToImage(canvas, canvasWidth, canvasHeight);
    
                updatePDFfile(canvas.toDataURL("image/jpeg"));
    
                
            });
        });
    
        function updatePDFfile(img){
            jQuery('.ajax-loader').show();
            var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
            
            jQuery.ajax({
                type: 'POST',   
                url: ajaxurl, 
                data: {imgData: img, action: "print_all_answers_graph"}, 
                success: function(data) {
                    
                    jQuery('.ajax-loader').hide();
                    window.open(
                      '<?php echo QUESTIONAIR_URL."assets/Mindful.pdf"; ?>',
                      '_blank' // <- This is what makes it open in a new window.
                    );
                    
                }
            });
        }
        
        $(".resetGraphAll").click(function(e) {
            jQuery('.ajax-loader').show();
             var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
            
            jQuery.ajax({
                type: 'POST',   
                url: ajaxurl, 
                data: {action: "reset_all_answers_graph"}, 
                success: function(data) {
                    jQuery('.ajax-loader').hide();
                    window.location.reload();
                    
                }
            });
        });

    </script>

<?php }else{ ?>

    <div class="alert alert-info">
        <p>Please login to view the content of this page.</p>
    </div>

<?php } ?>
