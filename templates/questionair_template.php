<?php

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;
$prefix = $wpdb->prefix;
$table_name = $prefix.'questionair';

$highest = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Highest Purpose and Vision'",""));

$planet = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Impact on the Planet'",""));

$society = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Impact on Society'",""));

$customer = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Customer and Community'",""));

$leadership = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Leadership and Culture'",""));

$employee = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Employee Engagement and Development'",""));

$promotion = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Promotion (internal and external)'",""));

$place = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Place (physical and digital)'",""));

$industry = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Efficiencies'",""));

$products = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Products and Services'",""));

$pricing = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Value Based Pricing'",""));

$investment = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `sub_type` = 'Mindful Investment'",""));

?>
<style type="text/css">
	/* Style the tab */
.tabupper {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tabupper button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tabupper button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tabupper button.activeques {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontentques {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.panel-heading{
	background: #f1f1f1;
}

.background-loader {
    background-color: hsl(0, 15%, 90%);
    display: block;
    height: 100%;
    left: 0;
    opacity: 0.3;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 10000;
}
.image-laoding {
    border-radius: 3px;
    height: auto;
    left: 50%;
    opacity: 0.99;
    padding: 3px;
    position: fixed;
    top: 40%;
    width: auto;
    z-index: 100001;
}
</style>
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<div class="container">
	<h2>The Mindful Index by The Mindful Collective</h2>
	<p>A visualisation tool that allows users to display their Mindful Index score</p>
	<div id="show_msg"></div>
	<div class="tabupper">
		<button class="tablinks" onclick="openCity(event, 'Purpose')">Mindful Purpose</button>
		<button class="tablinks" onclick="openCity(event, 'People')">Mindful People</button>
		<button class="tablinks" onclick="openCity(event, 'Process')">Mindful Process</button>
		<button class="tablinks" onclick="openCity(event, 'Profit')">Mindful Profit</button>
	</div>
<form method="post">
	<div id="Purpose" class="tabcontentques">
	  <div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
					<div class="panel-heading">
						<h4 class="panel-title">
							Highest Purpose and Vision
						</h4>
					</div>
				</a>
				<div id="collapse1" class="panel-collapse collapse in">
					<ul class="list-group">
						<?php 
					    $i = 1;
					    foreach ($highest as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="highest<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
					<div class="panel-heading">
						<h4 class="panel-title">
							Impact on the Planet
						</h4>
					</div>
				</a>
				<div id="collapse2" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($planet as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="planet<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
					<div class="panel-heading">
						<h4 class="panel-title">
							Impact on Society
						</h4>
					</div>
				</a>
				<div id="collapse3" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($society as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="society<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
		</div> 
	</div>

	<div id="People" class="tabcontentques">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
					<div class="panel-heading">
						<h4 class="panel-title">
							Customer and Community
						</h4>
					</div>
				</a>
				<div id="collapse4" class="panel-collapse collapse in">
					<ul class="list-group">
						<?php 
					    $i = 1;
					    foreach ($customer as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="customer<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
					<div class="panel-heading">
						<h4 class="panel-title">
							Leadership and Culture
						</h4>
					</div>
				</a>
				<div id="collapse5" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($leadership as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="leadership<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
					<div class="panel-heading">
						<h4 class="panel-title">
							Employee Engagement and Development
						</h4>
					</div>
				</a>
				<div id="collapse6" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($employee as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="employee<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
		</div> 
	</div>

	<div id="Process" class="tabcontentques">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
					<div class="panel-heading">
						<h4 class="panel-title">
							Promotion (internal and external)
						</h4>
					</div>
				</a>
				<div id="collapse7" class="panel-collapse collapse in">
					<ul class="list-group">
						<?php 
					    $i = 1;
					    foreach ($promotion as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="promotion<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
					<div class="panel-heading">
						<h4 class="panel-title">
							Place (physical and digital)
						</h4>
					</div>
				</a>
				<div id="collapse8" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($place as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="place<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
					<div class="panel-heading">
						<h4 class="panel-title">
							Efficiencies
						</h4>
					</div>
				</a>
				<div id="collapse9" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($industry as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="industry<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
		</div>
	</div>

	<div id="Profit" class="tabcontentques">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
					<div class="panel-heading">
						<h4 class="panel-title">
							Products and Services
						</h4>
					</div>
				</a>
				<div id="collapse10" class="panel-collapse collapse in">
					<ul class="list-group">
						<?php 
					    $i = 1;
					    foreach ($products as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="products<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
					<div class="panel-heading">
						<h4 class="panel-title">
							Value Based Pricing
						</h4>
					</div>
				</a>
				<div id="collapse11" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($pricing as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="pricing<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
					<div class="panel-heading">
						<h4 class="panel-title">
							Mindful Investment
						</h4>
					</div>
				</a>
				<div id="collapse12" class="panel-collapse collapse">
					<ul class="list-group">
				        <?php 
					    $i = 1;
					    foreach ($investment as $key => $value) {?>
					    	<li class="list-group-item"><input type="text" class="form-control" name="investment<?php echo $i ?>" value="<?php echo $value->question ?>" /></li>
					    <?php $i++;
					    } ?>
				    </ul>
				</div>
			</div>
		</div>
	</div>
</form>
<br>
<div class="row">
<div class="container">
	<button class="btn btn-primary pull-right" onclick="updateQuestions()">Update Questions</button>
</div>
</div>

<div class="ajax-loader" style="display:none;">
    <div class="image-laoding">
        <img src="<?php echo QUESTIONAIR_URL ?>assets/loader.gif" title="processing..." alt="loading..."/>
    </div>
    <div class="background-loader"></div>
</div>

<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontentques");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" activeques", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " activeques";
	}

	function updateQuestions(){
		jQuery('.ajax-loader').show();
		var allArr = ['highest', 'society', 'planet', 'customer', 'leadership', 'employee', 'promotion', 'place', 'industry', 'products', 'pricing', 'investment'];
        allArr.forEach(function(item, index, arr){
        	//for (var i = 10 - 1; i >= 0; i--) {
                var q1 = jQuery("input[name=" + item + "1]").val();
                q2 = jQuery("input[name=" + item + "2]").val(),
                q3 = jQuery("input[name=" + item + "3]").val(),
                q4 = jQuery("input[name=" + item + "4]").val(),
                q5 = jQuery("input[name=" + item + "5]").val(),
                q6 = jQuery("input[name=" + item + "6]").val(),
                q7 = jQuery("input[name=" + item + "7]").val(),
                q8 = jQuery("input[name=" + item + "8]").val(),
                q9 = jQuery("input[name=" + item + "9]").val(),
                q10 = jQuery("input[name=" + item + "10]").val();

                
            arr[item] = [q1 , q2 , q3 , q4 , q5 , q6 , q7 , q8 , q9 , q10];
        });
        console.log(allArr)

        var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";

        jQuery.ajax({
            type: 'POST',   
            url: ajaxurl, 
            data: jQuery('form').serialize() + "&action=update_all_questionair", 
            success: function(data) {
                jQuery('.ajax-loader').hide();
                if (data == 'done') {
                    
                    //alert('Successfully Updated');
                }else{
                    alert('Error');
                }
            }
        });
	}
</script>
	
</div>