var w = 500,
	h = 500;

var colorscale = d3.scale.category10();

//Legend titles
var LegendOptions = ['Smartphone','Tablet'];

//Data
var d = [
		  [
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.6},
			{axis:"",value:0.9},
			{axis:"",value:1},
			{axis:"",value:0.5},
			{axis:"",value:0.4},
			
		  ]
		];

//Options for the Radar chart, other than default
var mycfg = {
  w: w,
  h: h,
  maxValue: 1,
  levels: 0,
  ExtraWidthX: 300
}

//Call function to draw the Radar chart
//Will expect that data is in %'s
RadarChart.draw("#Questionairchart", d, mycfg);

////////////////////////////////////////////
/////////// Initiate legend ////////////////
////////////////////////////////////////////



//Create the title for the legend

		
//Initiate Legend	